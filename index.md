# Home
## Overview 
This tutorial was created to guide the Polygenic Risk Score estimative for admixed populations. 

Also, our intent is to introduce the use of partial polygenic score (pPS), ancestry specific partial polygenic score (aspPS) and combined ancestry polygenic score (casPS).

The definitions present in the [pPS aspPS casPS method paper](https://doi.org/10.1038/s41467-020-15464-w) comprise:

Partial polygenic score (pPS) as the total standardized polygenic score using only a subset of the genome.
Ancestry specific partial polygenic score (aspPS) as the polygenic score calculated on ancestry specific portions of a given genome.
Combined ancestry polygenic score (casPS) is obtained as the combination of two or more aspPS, weighting for p.

This tutorial is composed by three milestones:

1. [Milestone 1: being able to calculate aspPS and casPS using Davide's approach (and pipelines)](milestone1.md) 
2. Milestone 2: improve Davide's results by using balanced betas (i.e. betas calculated on a comparable number of individuals in each ancestry)
3. Milestone 3: apply the approach on novel data (Brazilians and/or others of interest)  + integrate caspPS with "environmental" covariates

The tutorial will be changed as the goals are achieved, also other schedules might be available 

!!! warning

    Data used in this tutorial are simulated and intended for demonstration purposes only. The results from this tutorial will not reflect the true performance of different software.

!!! note

    We assume you have basic knownledges on how to use the terminal, `plink`, `R` and 'Python'. 
    If you are unfamiliar with any of those, you can refer to the following online resources:

    | Software | link |
    |:-:|:-:|
    | terminal (OS X / Linux) |  [1](https://www.digitalocean.com/community/tutorials/basic-linux-navigation-and-file-management), [2](https://linuxconfig.org/bash-scripting-tutorial-for-beginners)
    | terminal (Windows)| [1](https://www.cs.princeton.edu/courses/archive/spr05/cos126/cmd-prompt.html), [2](https://www.tutorialspoint.com/batch_script/index.htm) |
    | plink | [v1.90](https://www.cog-genomics.org/plink/1.9/), [v1.75](http://zzz.bwh.harvard.edu/plink/) |
    | R | [1](https://www.tutorialspoint.com/r/index.htm)


!!! note

    This tutorial is written for Linux and OS X operating systems. 
    Windows users will need to change some commands accordingly.

!!! note
    Throughout the tutorial you will see tabs above some of the code:

    === "A"

        ```bash
        echo "Tab A"
        ```

    === "B"

        ```bash
        echo "Tab B"
        ```

    You can click on the tab to change to an alternative code (eg. to a different operation system)
## Datasets

1. Link1(ali)
2. Link2(acolá)

## Requirements
To follow the tutorial, you will need the following programs installed:

1. [R](https://www.r-project.org/) (**version 3.2.3+**)
2. [PLINK 1.9](https://www.cog-genomics.org/plink2)
3. Marnetto's package (https://bitbucket.org/dmarnetto/ancestry-specific-partial-ps/src/master/)

## Citation
If you find this tutorial helpful for a publication, then please consider citing:

!!! important "Citation"

    Marnetto, D., Pärna, K., Läll, K. et al. Ancestry deconvolution and partial polygenic score can improve susceptibility predictions in recently admixed individuals. Nat Commun 11, 1628 (2020). [https://doi.org/10.1038/s41467-020-15464-w]