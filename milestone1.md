

First check your files using the following command in a cluster:

snakemake --cluster sbatch -j5 -p UK_EURAFR.list

If doesn't work try to check the populations variable in config.sk and content of your pop_id input. 

To rule populationlist use the following command using a cluster:

snakemake --cluster sbatch -j5 -p UK_EURAFR.list UK_EUREAS.list UK_AFR.list UK_EAS.list UK_EUR.list UK_FAREUR.list

To rule vcf_from_list use first to check if everything is ok:

snakemake -np vcfs/1_UK_EUR.vcf.gz

If you are running your pipe in a cluster, try this:

module load vcftools-

snakemake --cluster sbatch -j5 -p vcfs/1_UK_EUR.vcf.gz

